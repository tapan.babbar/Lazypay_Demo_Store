$(document).ready(function() {

   $('#drop_down h6, #drop_down span').click(function() {
      $(this).parent().find('ul').slideDown();
   });

   $('#drop_down ul li').click(function() {
      $('#drop_down ul').slideUp();

      var text = $(this).text();
      $(this).parent().parent().find('h6').text(text);

   });




   $(".slider_icon").click(function() {
      var blockName = $(this).data('trigger');

      //console.log('#'+blockName)

      $("#category-list img").attr("src", "images/category/" + blockName + ".png");

      $(".slider_icon").find(".grey_icon").removeClass('deactive');
      $(this).find(".grey_icon").addClass('deactive');

      $(".slider_icon").find(".pink_icon").removeClass('active')
      $(this).find(".pink_icon").addClass('active')

      console.log($("#category-list img").attr("src"));
   });


   //console.log($(".slider_icon").data('trigger'))

   init();
   wow = new WOW({
      offset: 100
   })
   wow.init();


   $('.your-class').slick({
      slidesToShow: 5,
      arrows: true,
      autoplay: true,
      dots: false,
      slidesToScroll: 1,
      centerPadding: '60px',
      autoplay: false,
      asNavFor: '.slider-nav',
      responsive: [{
         breakpoint: 1024,
         settings: {
            slidesToShow: 3,
         }
      }, {
         breakpoint: 540,
         settings: {
            slidesToShow: 2,
         }
      }]


   });


   rotate_screen();



   $(".burger_menu").click(function() {
      $(".burger_menu_list").toggleClass('open');
      $(".burger_menu").toggleClass('open');

   });

   $(".benefit_button").click(function() {
      $(".brand_list").toggleClass("move");
      $(".benefit_button").toggleClass("rotated");

   });




   $('body').click(function(evt) {
      touch_check(evt)

      //Do processing of click event here for every element except with id menu_content

   });

   
   $(document).on('touchstart', function(event) {

      touch_check(event);

   });


   $("#begin_btn").click(function() {
      $("#demo_1").fadeOut(300);
      $("#demo_2").fadeIn(300);

   });

   $("#try_demo").click(function() {
      $("#demo_1").fadeIn(300);
      $(".demo_bg").fadeIn(300);

   });

   $("#continue_btn").click(function() {
      $("#demo_3").fadeOut(300);
      $(".demo_bg").fadeOut(300);

   });


   $("#confirm_btn").click(function() {
      $("#demo_3").fadeIn(300);
      $("#demo_2").fadeOut(300);

   });



   $("#know_more").click(function() {

      $("#know_more_pop").fadeIn(400);
   });

   $("#know_more_pop .pop_close").click(function() {

      $("#know_more_pop").fadeOut(400);
   });

   $("#details_tab").click(function() {

      $("#benefits_pop").fadeIn(400);
   });

   $("#benefits_pop .pop_close").click(function() {

      $("#benefits_pop").fadeOut(400);
   });

   $("#steps #steps_info").click(function() {
      $("#repayment-pop").fadeIn(400);
   });

   $("#repayment-pop .pop_close").click(function() {
      $("#repayment-pop").fadeOut(400);
   });

   $("#read_more").click(function() {
      $("#benefits_time_pop").fadeIn(400);
   });

   $("#benefits_time_pop .pop_close").click(function() {
      $("#benefits_time_pop").fadeOut(400);
   });

});

$(window).resize(function(e) {
   init();
   rotate_screen();
});


function rotate_screen() {
   if (is_touch_device() && window.innerWidth > window.innerHeight && window.innerWidth < 700) {
      $(".rotate_screen").show();
   } else {
      $(".rotate_screen").hide();
   }
}

function is_touch_device() {
   try {
      document.createEvent("TouchEvent");
      return true;
   } catch (e) {
      return false;
   }
}


function init() {


      if (window.innerWidth < 1100 && window.innerWidth > 768) {
         $(".brand_list_li").css({
            "height": $("#benefits .col-md-4").height() / 3
         });
         console.log("yes")
      } else {

         $(".brand_list_li").css({
            "height": $("#benefits .col-md-4").height() / 2
         });

      }

   }


function touch_check(evt) {

   if (evt.target.id == "try_demo")
      return;

   if ($(evt.target).closest('#try_demo').length)
      return;

   if (evt.target.id == "demo_1")
      return;
   //For descendants of menu_content being clicked, remove this check if you do not want to put constraint on descendants.
   if ($(evt.target).closest('#demo_1').length)
      return;

   if (evt.target.id == "demo_2")
      return;
   //For descendants of menu_content being clicked, remove this check if you do not want to put constraint on descendants.
   if ($(evt.target).closest('#demo_2').length)
      return;

   if (evt.target.id == "demo_3")
      return;
   //For descendants of menu_content being clicked, remove this check if you do not want to put constraint on descendants.
   if ($(evt.target).closest('#demo_3').length)
      return;


   /**/

   $(".demo").fadeOut(300);
   $(".demo_bg").fadeOut(300);
}