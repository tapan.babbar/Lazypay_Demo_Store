//Read only the number keys on keyboard
function onlyNumbers(event){
	if ( (event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) ) // 0-9 or numpad 0-9
		{
			// check textbox value now and tab over if necessary
		}
	else if (event.keyCode !== 9 && event.keyCode !== 46 && event.keyCode !== 37 && event.keyCode !== 39 && event.keyCode !== 8) // not esc, del, left or right
		{
			event.preventDefault();
		}
			// else the key should be handled normally
	//console.log("click");		
}

function init() {


      if (window.innerWidth < 1100 && window.innerWidth > 768) {
         $(".brand_list_li").css({
            "height": $("#benefits .col-md-4").height() / 3
         });
      } else {

         $(".brand_list_li").css({
            "height": $("#benefits .col-md-4").height() / 2
         });

      }

   }
function is_touch_device() {
  return 'ontouchstart' in window // works on most browsers 
      || 'onmsgesturechange' in window; // works on ie10
};

$(document).ready(function() {


   $('#drop_down h6, #drop_down span').click(function() {
      $(this).parent().find('ul').slideDown();
   });

   $('#drop_down ul li').click(function() {
      $('#drop_down ul').slideUp();

      var text = $(this).text();
      $(this).parent().parent().find('h6').text(text);

   });


   init();

	if (is_touch_device()) {
	
	}
	else
	{
   wow = new WOW({
      offset: 100
   })
   wow.init();
	}

   $('.your-class').slick({
      slidesToShow: 1,
      arrows: false,
      autoplay: true,
      dots: true,
      slidesToScroll: 1,
      centerPadding: '60px',
      asNavFor: '.slider-nav'
   });


   rotate_screen();




   $(".burger_menu").click(function() {
      $(".burger_menu_list").toggleClass('open');
      $(".burger_menu").toggleClass('open');

   });

   $(".benefit_button").click(function() {
      $(".brand_list").toggleClass("move");
      $(".benefit_button").toggleClass("rotated");

   });




   $('body').click(function(evt) {
      touch_check(evt)

      //Do processing of click event here for every element except with id menu_content




   });

   

   $(document).on('touchstart', function(event) {

      touch_check(event);

   });


   $("#begin_btn").click(function() {
      $("#demo_1").fadeOut(300);
      $("#demo_2").fadeIn(300);

   });

   $("#try_demo").click(function() {
      $("#demo_1").fadeIn(300);
      $(".demo_bg").fadeIn(300);

   });
      $("#try_demo_2").click(function() {
      $("#demo_1").fadeIn(300);
      $(".demo_bg").fadeIn(300);

   });

   $("#continue_btn").click(function() {
      $("#demo_3").fadeOut(300);
      $(".demo_bg").fadeOut(300);

   });


   $("#confirm_btn").click(function() {
      $("#demo_3").fadeIn(300);
      $("#demo_2").fadeOut(300);

   });

   $("#coperate_signup_pop").click(function(){
      //console.log('click')
	  $(".corp_sign_form").addClass("active") ;
   });
   
   $(".corp_sign_form .pop_close").click(function(){
      //console.log('click')
	  $(".corp_sign_form").removeClass("active") ;
   });

   $(".corp_sign_form .form_content form .form-group .col-lg-12 #company-list").click(function(){
		$(this).children().eq(0).hide();
	
	   	//var pickValue = $(this).data('address');
   		//console.log('click');
		$(this).on('change', function(){ 
			var pickValue = $(this).val();
			
			$(".corp_sign_form .form_content form .form-group input.data-input").val(pickValue);
		});
   });
 


   $("#know_more").click(function() {

      $("#know_more_pop").fadeIn(400);
   });

   $("#know_more_pop .pop_close").click(function() {

      $("#know_more_pop").fadeOut(400);
   });

   $("#details_tab").click(function() {

      $("#benefits_pop").fadeIn(400);
   });

   $("#benefits_pop .pop_close").click(function() {

      $("#benefits_pop").fadeOut(400);
   });

   $("#steps #steps_info").click(function() {
      $("#repayment-pop").fadeIn(400);
   });

   $("#repayment-pop .pop_close").click(function() {
      $("#repayment-pop").fadeOut(400);
   });

   $("#read_more").click(function() {
      $("#benefits_time_pop").fadeIn(400);
   });

   $("#benefits_time_pop .pop_close").click(function() {
      $("#benefits_time_pop").fadeOut(400);
   });

});

$(window).resize(function(e) {
 //  init();
   rotate_screen();
});


function rotate_screen() {
   if (is_touch_device() && window.innerWidth > window.innerHeight && window.innerWidth < 700) {
      $(".rotate_screen").show();
   } else {
      $(".rotate_screen").hide();
   }
}

function is_touch_device() {
   try {
      document.createEvent("TouchEvent");
      return true;
   } catch (e) {
      return false;
   }
}

function touch_check(evt) {

   if (evt.target.id == "try_demo")
      return;

   if (evt.target.id == "try_demo_2")
   return;	  

   if ($(evt.target).closest('#try_demo').length)
      return;
	  
    if ($(evt.target).closest('#try_demo_2').length)
      return;
	  

   if (evt.target.id == "demo_1")
      return;
   //For descendants of menu_content being clicked, remove this check if you do not want to put constraint on descendants.
   if ($(evt.target).closest('#demo_1').length)
      return;

   if (evt.target.id == "demo_2")
      return;
   //For descendants of menu_content being clicked, remove this check if you do not want to put constraint on descendants.
   if ($(evt.target).closest('#demo_2').length)
      return;

   if (evt.target.id == "demo_3")
      return;
   //For descendants of menu_content being clicked, remove this check if you do not want to put constraint on descendants.
   if ($(evt.target).closest('#demo_3').length)
      return;


   /**/

   $(".demo").fadeOut(300);
   $(".demo_bg").fadeOut(300);
}